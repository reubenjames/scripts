#!/bin/bash

# Create ssh key
ssh-keygen -f id_rsa -t rsa -N ''

# Install basic packages and simpleSH tool
sudo apt-get install -y \
	bash-completion wget curl vim

# Install simpleSH tool for further bootstrapping
sudo apt-get install -y wget curl unzip
wget -qO- -O simplesh.zip https://github.com/rafaelstz/simplesh/archive/master.zip
unzip simplesh.zip && rm simplesh.zip
cd simplesh-master/
bash simple.sh

# Install Docker CE
sudo apt-get remove docker docker-engine docker.io;
sudo apt-get update;
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common;
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -;
sudo apt-key fingerprint 0EBFCD88;
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable";
sudo apt-get update;
sudo apt-get install docker-ce;
sudo groupadd docker;
sudo usermod -aG docker $USER;

# Install Docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose;
sudo chmod +x /usr/local/bin/docker-compose;
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose;
docker-compose --version;
