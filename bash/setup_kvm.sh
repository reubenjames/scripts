#!/bin/bash

# Checks
egrep -c '(vmx|svm)' /proc/cpuinfo
#check outcome of previous cmd is greater than 0 ot continue otherwise throw error

# Pkgs
sudo apt install -y qemu qemu-kvm libvirt-daemon libvirt-clients bridge-utils virt-manager
sudo systemctl enable --now libvirtd
lsmod | grep -i kvm
