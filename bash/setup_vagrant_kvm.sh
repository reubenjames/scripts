#!/bin/bash

# Pkgs
sudo apt -y install qemu libvirt-daemon-system libvirt-clients libxslt-dev libxml2-dev libvirt-dev zlib1g-dev ruby-dev ruby-libvirt ebtables dnsmasq-base
cd /tmp/
curl -O https://releases.hashicorp.com/vagrant/2.2.9/vagrant_2.2.9_x86_64.deb
sudo apt install /tmp/vagrant_2.2.9_x86_64.deb
cd -
vagrant --version
vagrant plugin install vagrant-libvirt
vagrant plugin install vagrant-mutate
export VAGRANT_DEFAULT_PROVIDER=libvirt >> ~/.profile
