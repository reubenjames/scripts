#!/bin/bash

# Pkgs
sudo add-apt-repository ppa:lutris-team/lutris
sudo add-apt-repository ppa:graphics-drivers/ppa
sudo dpkg --add-architecture i386
sudo apt update

# Install wine
sudo apt install nvidia-driver-450 libvulkan1 libvulkan1:i386
sudo wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo apt-key add winehq.key
sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ focal main'
sudo apt update && sudo apt install --install-recommends winehq-stable

# Install lutris
sudo apt install lutris -y
echo export LC_ALL=C >> ~/.profile
echo
echo #####################################################################
echo Lutris installed. You will need to Reboot for changes to take effect. 
echo #####################################################################
echo
